$(function () {
	$('form').on('submit', function(e){e.preventDefault();}); /* Запрещаем для всех форм дефолтное событие submit */
	var smsCodeTimerOptions = { /* Опции для таймера */
			timerEndFn: function() { /* Функция, вызывающаяся по окончании таймера и кол-во попыток */
				$('.js-form-txt').text('Вы исчерпали кол-во попыток получения смс-кода.');
			},
			sendCodeBtnTemplate: '<a href="#" class="js-counter-btn c-sms-counter__btn">Выслать код повторно</a>', /* Верстка для кнопки повторной отправки кода */
			elParent: $('.js-counter'), /* Элемент счетчика */
			el: $('.js-counter-number'), /* Элемент кол-во секунд счетчика */
			seconds: 60, /* Кол-во секунд */
			repeats: 3, /* Кол-во попыток получения кода */
		}, 
		userData = JSON.parse(localStorage.getItem('userData')), /* Данные пользователя из localStorage */
		codeAttempts = 3; /* Кол-во попыток ввода кода */
		

		$('.js-form-field-code').mask('00000'); /* Маска для поля кода */
		$('.js-form-field-phone').mask('+7 (000) 000-00-00'); /* Маска для поля телефона */

		if(userData){
			if(!userData.codeSentTimeStart) { /* Если нет временной метки первого попадания на страницу ввода кода */
				userData.codeSentTimeStart = (new Date()).getTime(); /*Создаем новую*/
				localStorage.setItem('userData', JSON.stringify(userData));
			}else{
				if(((new Date).getTime() - userData.codeSentTimeStart) >= 24 * 60 * 60 * 1000){ /* Если с момента первого попадания на страницу ввода кода прошло более 24 часов */
					$('.js-form-txt').text('Прошло больше 24 часов с момента отправки кода');
					$('.js-form-submit').remove(); 
		    		$('.js-counter').remove(); 
				}
			}
		}else{ /* Если нет данных в localStorage, создаем, с временной меткой первого попадания на страницу ввода кода */
			localStorage.setItem('userData', JSON.stringify({codeSentTimeStart: (new Date()).getTime()}));
		}

	$('#form-code .js-form-submit').click(function(){
	        var submitBtn = $(this);
	        var codeValue = $(this).parent().find('.js-form-field-code').val();
	        sendSmsCode({'code': codeValue}).done(function(response){
	        	if(response.success){
					$('.js-form-txt').text('Регистрация прошла успешно');
	        	}
		    }).fail(function(response){
		    	$('.js-form-txt').text(response.responseJSON.message);
		    	codeAttempts--;
		    	if(codeAttempts == 0){
		    		$('.js-form-txt').text('Вы исчерпали кол-во попыток ввода смс-кода. Попробуйте завтра.');
		    		submitBtn.remove();
		    		$('.js-counter-btn').remove();
		    	}
		    })
		    return false;
	    });



	$(document).on('click', '.js-counter-btn', function(e){ /* Обработчик для кнопки "Отправить код повторно" */
			e.preventDefault(); /* Запрещаем дефолтное событие (переход по ссылке) */
			var btn = $(this); /* Ссылка на тек. кнопку */
			getSmsCode({'phone': numberFormat(userData.phone)}).done(function(){
				btn.remove(); 
				smsCodeTimerOptions.el.text(smsCodeTimerOptions.seconds); /* Задаем начальное кол-во секунд таймера (60) */
				smsCodeTimerOptions.el.show(); /* Показываем элемент кол-ва секунд таймера */
				startTimer(smsCodeTimerOptions); /* Запускаем таймер с параметрами */
			}).fail(function(response) {
				btn.remove(); 
				$('.js-form-txt').text(response.responseJSON.message);
			})
		})

	$('.js-form-field-code, .js-form-field-phone').on('input', function(){ /* Обработчик полей ввода */
		if(!$('.js-form-txt').text()) return;
		$('.js-form-txt').text(''); /* Если в элементе ошибки формы есть текст, то очищаем */
	})

	$('#form-phone .js-form-submit').click(function(){ /* Обработчик получения смс-кода */
        var phoneValue = $(this).parent().find('.js-form-field-phone').val(); /* Введенный телефон */
        if(numberFormat(phoneValue).length < 11){ /* Оставляем только числа из строки с телефоном и проверяем на кол-во */
	        $('.js-form-txt').text('Неверный формат номера');
	        return;
        }

        getSmsCode({'phone': numberFormat(phoneValue)}).done(function(response){ /* Получаем смс-код */
			/* 
			* Записываем в localStorage телефон пользователя, 
			* чтобы потом обратиться к нему при отправке кода
			*/
			if(response.success){
				/* Заносим в localStorage телефон и информацию о том, что код отправлен */
				localStorage.setItem('userData', JSON.stringify({phone: phoneValue, codeSended: true, codeSentTimeStart: (new Date()).getTime()})); 
				smsCodeTimerOptions.el.text(smsCodeTimerOptions.seconds); /* Задаем начальное кол-во секунд таймера (60) */
				startTimer(smsCodeTimerOptions); /* Запускаем таймер с параметрами */
				$('#form-phone').hide();
				$('#form-code').show();
			}
			if(response.error_text){
				$('.js-form-txt').text(response.error_text);
			}
		}).fail(function(response) {
			$('.js-form-txt').text(response.responseJSON.message);
		})
    });
    
});
