var 
sendSmsCodeOptions = {
	url: 'index.php?action=confirm',
	method: 'POST'
},
getSmsCodeOptions = {
	url: 'index.php?action=send-code',
	method: 'POST'
},
timerDefaultOptions = {
	btnTemplate: '<a href="#" class="js-counter-btn">Выслать код повторно</a>'
};

function sendAjax(aData, $url, $method) { /* Обертка над методом ajax */
    return $.ajax({
        url: $url, /* url страницы (action_ajax_form.php) */
        type: $method, /* метод отправки */
        dataType: "json", /* формат данных */
        data: aData,  /* данные для отправки */
    });
}

function sendSmsCode(data){ /* Обертка над отправкой смс кода методом ajax */
	return sendAjax(data, sendSmsCodeOptions.url, sendSmsCodeOptions.method);
}

function getSmsCode(data){ /* Обертка над получением смс кода методом ajax */
	return sendAjax(data, getSmsCodeOptions.url, getSmsCodeOptions.method)
}

function numberFormat(string){ /* Функция возвращающая только цифры из переданной строки */
	return string.replace(/\D/g, '');
}

function startTimer(options){ /* Функция запуска таймера */
	var seconds = options.seconds;
	var handler = setInterval(function(){ 
		seconds--;
		if(seconds == 0){ 
			$(options.el).hide(); /* Если время вышло, скрываем элемент кол-во секунд таймера */
			seconds = options.seconds; /* Присваиваем нашей созданной, отдельно, переменной с секундами, начальное значение из параметров */
			options.repeats--; /* Уменьшаем вол-во попыток получаения кода на 1 */
			if(options.repeats > 0){ 
				$(options.elParent).prepend(options.sendCodeBtnTemplate || timerDefaultOptions.btnTemplate); /*  */
			}else{
				options.timerEndFn(); /* Вызываем функцию окончания таймера по окончанию кол-во попыток */
			}
			clearInterval(handler); /* Очищаем таймер по окончанию времени */
		} 
		$(options.el).text(seconds); /* Изменяем кол-во секунд в элементе таймера для кол-во секунд */
	}, 1000);
}