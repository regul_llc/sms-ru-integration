<?php

require 'backend/reg.php';
$action = (isset($_GET['action']) && !empty($_GET['action'])) ?$_GET['action'] : '' ; //получаем метод (send-code или confirm)
(new Router())->route($action);

class Router
{
    /**
     * Роутинг
     * @param string $action
     * @return string
     */
    public  function route($action){
        $oReg = new Reg();
        switch ($action){
            case 'send-code' : {
                $result = $oReg->sendCode();
                break;
                }
            case 'confirm' : {
                $result = $oReg->confirm();
                break;
            }
            default : {
                    $result = ['success' => false, 'message' => 'Метод не существует'];
                    http_response_code(405);
                break;
            }
        }
        echo json_encode($result);
        return json_encode($result);
    }
}

