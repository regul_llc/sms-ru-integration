<?php

require 'sms.php';
require 'project.php';

 class Reg{


     protected $limitAttemptConfirmCount;
     protected $limitSmsCount;
     protected $oProject;

     public function __construct()
     {
         $oConfig = new Config();
         $aConfig= $oConfig->configure();
         $this->limitAttemptConfirmCount = (isset($aConfig['LimitAttemptConfirmCount'])? $aConfig['LimitAttemptConfirmCount'] : 3); ////Количество возможных попыток подтверждения смс
         $this->limitSmsCount = (isset($aConfig['LimitSmsCount'])? $aConfig['LimitSmsCount'] : 3); //Количество возможных попыток отправки смс
         $this->oProject = new Project();
     }
     /**
      * Метод подтверждения кода из смс
      * @return array
      */
    public function confirm(){
        $phone = $this->getPhone();
        $codeSms = (isset($_POST['code']) && !empty($_POST['code'])) ? $_POST['code'] : 0;
        $this->oProject->saveAttemptConfirmCount($phone); //сохранение количества попыток подтверждения кода
        $codes = $this->oProject->getCodes($phone); //получить коды пользователя за отпределенное время
        $success = false;
        foreach ($codes as $code){
            if ($codeSms == $code){
                $success = true; //код найден
                $this->oProject->setStatusConfirmed($phone); //добавление в бд записи со статусом 'Подтвержден'
            }
        }
        if($success == false){
            $result = ['success' => false, 'message' => 'Переданный код и смс код не совпадают'];
            http_response_code(403);
        }else{
            $result = ['success' => true,'message' => 'Переданный код и смс код совпадают'];
        }
        $attemptConfirmCount = $this->oProject->getAttemptConfirmCount($phone); //Получение количества попыток подтверждения кода смс
        if($attemptConfirmCount >= $this->limitAttemptConfirmCount ){
            $this->oProject->setConfirmError($phone); //В БД заносится информация об ошибке, что пользователь не подтвердил данные.
        }
        return $result;
    }

     /**
      * Получение телеофона из cookie
      * @return string
      *
      */
    public function getPhone()
    {
        return (isset($_COOKIE['phone']) && !empty($_COOKIE['phone'])) ? $_COOKIE['phone'] : '';
    }

     /**
      * Метод отправки сообщения с кодом
      * @return array
      */
    public function sendCode(){
        $phone = (isset($_POST['phone']) && !empty($_POST['phone']))? $_POST['phone'] : '';
        $this->setCookiePhone($phone);
        $smsCount = $this->oProject->getSmsCount(); //получаем количество отправленых смс пользователя
        if($smsCount >= $this->limitSmsCount ){
            $this->oProject->setLimitError(); //заносим информацию в БД что лимит исчерпан
            $result =  ['success' => false, 'message' => 'Лимит сообщений исчерпан'];
            http_response_code(403);
        }else{
            $checkAvailableSendSmsByTime = $this->oProject->checkAvailableSendSmsByTime();
            if($checkAvailableSendSmsByTime == true) {
                $code = $this->generateCode(); //генерируем код
                $this->oProject->saveParams($code); //сохраняем код и количесвто отправленных кодов
                $result = (new Sms())->sendSms($phone, (string)$code); //отправка сообщения
                $this->oProject->setStatusNotConfirmed(); //запись в БД со статусом 'Не подтвержден'
            }else{
                $result =  ['success' => false, 'message' => 'Интервал между отправками смс превышен'];
                http_response_code(403);
            }
        }
        return $result;
    }

     /**
      * Запись телефона в cookie
      * @param string $phone
      */
    protected function setCookiePhone($phone){
        setcookie('phone', $phone);
    }
     /**
      * @return integer
      */
     protected function generateCode(){
         return mt_rand(10000, 99999) ;
     }
 }

?>