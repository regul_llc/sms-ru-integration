<?php

require 'config.php';

class Sms
{
    /**
     * Класс для взаимодействия с sms.ru
     *
     */

    protected $apiId;

    public function __construct()
    {
        $oConfig = new Config();
        $aConfig= $oConfig->configure();
        $this->apiId = (isset($aConfig['ApiId'])? $aConfig['ApiId'] : ''); // Уникальный ключ - находится на главной странице личного кабинета sms.ru
    }

    /**
     * Сбор данных перед отправкой
     * @param string $phone
     * @param string $code
     * @return array
     */
    public function sendSms($phone, $code){
        $aData = $this->createSmsData($phone, $code); //получем массив с данными
        $sData = http_build_query($aData);
        $sAnswer = $this->curl($sData);
        return $sAnswer;
    }

    /**
     * Отправка смс
     * @param  string $sData (данные формируются в методе createSmsData)
     * @return array
     */
    private function curl($sData)
    {
       $result =[];
        $ch = curl_init("https://sms.ru/sms/send"); //url для sms.ru
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);
        $body = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($body);
        if ($json) { // Получен ответ от сервера
            if ($json->status == "OK") { // Запрос выполнился
                foreach ($json->sms as $phone => $data) { // Перебираем массив СМС сообщений
                    if ($data->status == "OK") { // Сообщение отправлено
                        $result = ['success' => true, 'phone' =>$phone, 'id_message' =>$data->sms_id ];
                    } else { // Ошибка в отправке
                        http_response_code($data->status_code);
                        $result = ['success' => false, 'error_code' =>$data->status_code, 'phone' =>$phone,'error_text' =>$data->status_text];
                    }
                }
            } else { // Запрос не выполнился (возможно ошибка авторизации, параметрах, итд...)
                http_response_code($json->status_code);
                $result = ['success' => false, 'error_code' =>$json->status_code, 'error_text' => $json->status_text];
            }
        } else {
            http_response_code(404);
            $result =['success' => false, 'error_code' =>404, 'error_text' => "Запрос не выполнился. Не удалось установить связь с сервером. "];
        }
        return $result;
        //return ['success' => true];
    }

    /**
     * Создание массива с необходимыми данными для отправки
     * @param string $phone
     * @param string $strText
     * @return array
     */
    private function createSmsData($phone, $strText)
    {
        $aData = [
            "api_id" => $this->apiId, //Авторизация по уникальному ключу. Уникальный ключ находится на главной странице личного кабинета sms.ru
            "to" => $phone, //Номер телефона получателя
            "msg" =>  $strText, //Текст сообщения в кодировке UTF-8
            "json" => 1 //Вызывает ответ сервера в формате JSON
        ];
        return $aData;
    }
}
