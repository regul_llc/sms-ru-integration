<?php


class Config
{
    /**
     * @return array
     * ApiId - ключ для внешних программ
     * Заменить ApiId на ваш api_id из sms.ru!!
     */
    public function configure(){
        $config = [
            'ApiId' => '27AE246D-0C28-EEB7-A5CF-6C6DD4505107', //Уникальный ключ - находится на главной странице личного кабинета sms.ru
            'LimitAttemptConfirmCount' => 3, ////Количество возможных попыток подтверждения смс
            'LimitSmsCount' => 3, //Количество возможных попыток отправки смс
        ];
        return $config;
    }
}
