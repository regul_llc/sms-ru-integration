<?php

class Project
{
    /**
     * Класс для методов, которые будут заменены на методы работы в БД
     *
     */


    /**
     * Сохранение количества попыток отправки кода в куках
     *Заменить на сохранение количества попыток отправки кода в БД !!
     * @param string $phone для хранения в БД
     */
    public function saveAttemptConfirmCount($phone){
        setcookie("confirm-count", isset($_COOKIE['confirm-count']) ? $_COOKIE['confirm-count']+1 : 1);
    }

    /**
     * Создание записи в БД с данными пользователя. Статус "Подтвержден"
     * Добавить реализацию !!
     * @param string $phone для хранения в БД
     */
    public function setStatusConfirmed($phone){

    }

    /**
     * Получение отправленых кодов пользователя
     * Заменить на получение массива кодов из БД за определенное время!!
     * @param string $phone для хранения в БД
     * @return array
     */
    public function getCodes($phone){
       $time = $this->getTimeSaveCode(); //время хранения кода, будет использоваться при получении кодов из БД
       return $codes = isset($_COOKIE['code']) ? $_COOKIE['code'] : [];
    }

    /**
     * Получение количества попыток отправки кода
     * Получить количество попыток отправки кода из БД !!
     * @param string $phone для хранения в БД
     * @return integer
     */
    public function getAttemptConfirmCount($phone){
        return (isset($_COOKIE['confirm-count']) && !empty($_COOKIE['confirm-count'])) ? $_COOKIE['confirm-count'] : 0;
    }

    /**
     *  В БД заносится информация об ошибке, что пользователь не подтвердил данные.
     * Добавить реализацию !!
     * @param string $phone для хранения в БД
     */
    public function setConfirmError($phone){

    }

    /**
     * Сохранение кода и количества отправленых кодов в БД
     * Заменить на сохранение этих параметров в БД!!
     * @param string $code
     */
    public function saveParams($code){
        $time = $this->getTimeSaveCode();
        setcookie('code['.$code.']',$code,$time );
        setcookie("sms-count", isset($_COOKIE['sms-count']) ? $_COOKIE['sms-count']+1 : 1);
    }

    /**
     * Заменить время хранения смс при необходимости
     * @return integer
     *
     */
    protected function getTimeSaveCode(){
        return time()+3600*24;
    }

    /**
     * Получение количества отправленных смс
     * Заменить на получение количества отправленных смс из БД
     * @return integer
     */
    public function getSmsCount(){
        return (isset($_COOKIE['sms-count']) && !empty($_COOKIE['sms-count'])) ? $_COOKIE['sms-count'] : 0;
    }

    /**
     * В БД заносится информация об ошибке, что лимит отправки сообщения у пользователя исчерпан
     * Добавить реализацию !!
     */
    public function setLimitError(){

    }

    /**
     * Создание записи в БД с данными пользователя. Статус "Не подтвержден"
     * Добавить реализацию !!
     */
    public function setStatusNotConfirmed(){

    }

    /**
     * Здесь предполагается логика проверки времени отправки последнего сообщения по БД.
     * Данный таймер реализовано на фронте. проверка в данном методе служит для безопасности. если в этом нет необходимости - оставить как есть
     * @return bool
     */
    public function checkAvailableSendSmsByTime(){
        return true;
    }
}